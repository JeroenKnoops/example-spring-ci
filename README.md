# Example project - ci - Spring boot

## Purpose

This project is an example how to use the ci scripts from https://gitlab.com/jeroenknoops/ci

The java source of the project is based on the default [Spring Boot Example](https://spring.io/guides/gs/spring-boot/)

## Run

Look at the pipeline in gitlab and the `gitlab-ci.yml` file

## Outputs


- Test report: https://jeroenknoops.gitlab.io/example-spring-ci/
- Test summary: https://gitlab.com/JeroenKnoops/example-spring-ci/merge_requests/1  



## License

MIT
